﻿using EulerLogic.IManagers;
using Microsoft.AspNetCore.Mvc;

namespace Euler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Problem6Controller : ControllerBase
    {
        private readonly IProblem6Manager _problem6Manager;

        public Problem6Controller(IProblem6Manager problem6Manager)
        {
            _problem6Manager = problem6Manager;
        }

        [HttpPost]
        public long Post(int n)
        {
            return _problem6Manager.DifferenceBetweenSumAndSquareOfFirstXNaturalNumbers(n);
        }
    }
}
