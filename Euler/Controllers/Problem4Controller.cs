﻿using Microsoft.AspNetCore.Mvc;
using EulerLogic.IManagers;

namespace Euler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Problem4Controller : ControllerBase
    {
        private readonly IProblem4Manager _problem4Manager;

        public Problem4Controller(IProblem4Manager problem4Manager)
        {
            _problem4Manager = problem4Manager;
        }

        [HttpPost]
        public long Post(int n)
        {
            return _problem4Manager.LargestPalindromeProductOfTwoNDigitNumbers(n);
        }
    }
}

