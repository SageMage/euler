﻿using EulerLogic.IManagers;
using Microsoft.AspNetCore.Mvc;

namespace Euler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Problem8Controller : ControllerBase
    {
        private readonly IProblem8Manager _problem8Manager;

        public Problem8Controller(IProblem8Manager problem8Manager)
        {
            _problem8Manager = problem8Manager;
        }

        [HttpPost]
        public ulong Post(ulong n)
        {
            return _Problem8Manager.LargestProductInSeries(n);
        }
    }
}
