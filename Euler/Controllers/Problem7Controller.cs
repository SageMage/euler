﻿using EulerLogic.IManagers;
using Microsoft.AspNetCore.Mvc;

namespace Euler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Problem7Controller : ControllerBase
    {
        private readonly IProblem7Manager _problem7Manager;

        public Problem7Controller(IProblem7Manager problem7Manager)
        {
            _problem7Manager = problem7Manager;
        }

        [HttpPost]
        public ulong Post(ulong n)
        {
            return _problem7Manager.NthPrime(n);
        }
    }
}
