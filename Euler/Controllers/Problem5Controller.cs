﻿using EulerLogic.IManagers;
using Microsoft.AspNetCore.Mvc;

namespace Euler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Problem5Controller : ControllerBase
    {
        private readonly IProblem5Manager _problem5Manager;

        public Problem5Controller(IProblem5Manager problem5Manager)
        {
            _problem5Manager = problem5Manager;
        }

        [HttpPost]
        public ulong Post(ulong n)
        {
            return _problem5Manager.SmallestMultipleOfNumbersBetween1AndN(n);
        }
    }
}
