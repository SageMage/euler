﻿using EulerLogic.IManagers;
using Microsoft.AspNetCore.Mvc;

namespace Euler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Problem3Controller : ControllerBase
    {
        private readonly IProblem3Manager _problem3Manager;

        public Problem3Controller(IProblem3Manager problem3Manager)
        {
            _problem3Manager = problem3Manager;
        }

        [HttpPost]
        public ulong Post(ulong n)
        {
            return _problem3Manager.LargestPrimeFactorOfN(n);
        }
    }
}
