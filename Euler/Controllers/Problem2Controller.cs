﻿using EulerLogic.IManagers;
using Microsoft.AspNetCore.Mvc;

namespace Euler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Problem2Controller : ControllerBase
    {
        private readonly IProblem2Manager _problem2Manager;

        public Problem2Controller(IProblem2Manager problem2Manager)
        {
            _problem2Manager = problem2Manager;
        }

        [HttpPost]
        public long Post(int x)
        {
            return _problem2Manager.SumEvenFibonacciNumbersLessThanN(x);
        }
    }
}
