﻿using EulerLogic.IManagers;
using Microsoft.AspNetCore.Mvc;

namespace Euler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Problem1Controller : ControllerBase
    {
        private readonly IProblem1Manager _problem1Manager;

        public Problem1Controller(IProblem1Manager problem1Manager)
        {
            _problem1Manager = problem1Manager;
        }

        [HttpPost]
        public long Post(int x, int y, int max)
        {
            return _problem1Manager.CalculateSumOfMultiples(x, y, max);
        }
    }
}
