﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;
using EulerLogic.IEngines;

namespace EulerLogic.Engines
{
    public class PrimeEngine : IPrimeEngine
    {
        public bool IsPrime(ulong number)
        {
            var result = true;
            if (number == 1)
                result = true;
            else if (number < 4)
                result = true;
            else if (number % 2 == 0)
            {
                result = false;
            }
            else if (number < 9)
            {
                result = true;
            }
            else if (number % 3 == 0)
            {
                result = false;
            }
            else
            {
                var r = ulong.Parse(Math.Floor(Math.Sqrt(number)).ToString(CultureInfo.InvariantCulture));
                ulong f = 5;
                while (f <= r)
                {
                    if (number % f == 0)
                        result = false;
                    else if (number % f + 2 == 0)
                        result = false;

                    if (!result)
                    {
                        break;
                    }

                    f += 6;
                }
                
            }

            return result;
        }

        public IEnumerable<ulong> GenerateEnumerableOfPrimes(ulong n)
        {
            var result = new List<ulong> {2};

            for (ulong i = 3; result.Count < (int)n; i+=2)
            {
                if (IsPrime(i))
                {
                    result.Add(i);
                }
            }

            return result;
        }
    }
}