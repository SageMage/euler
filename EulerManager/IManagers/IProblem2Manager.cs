﻿namespace EulerLogic.IManagers
{
    public interface IProblem2Manager
    {
        public long SumEvenFibonacciNumbersLessThanN(int n);
    }
}