﻿namespace EulerLogic.IManagers
{
    public interface IProblem6Manager
    {
        public long DifferenceBetweenSumAndSquareOfFirstXNaturalNumbers(int x);
        public long SumOfTheSquaresOfFirstXNaturalNumbers(int x);
        public long SquareOfTheSumOfFirstXNaturalNumbers(int x);
    }
}