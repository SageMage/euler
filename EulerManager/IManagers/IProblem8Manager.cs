﻿namespace EulerLogic.IManagers
{
    public interface IProblem8Manager
    {
        public ulong LargestProductInSeries(ulong n);
    }
}