﻿namespace EulerLogic.IManagers
{
    public interface IProblem7Manager
    {
        public ulong NthPrime(ulong n);
    }
}