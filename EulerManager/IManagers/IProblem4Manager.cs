﻿namespace EulerLogic.IManagers
{
    public interface IProblem4Manager
    {
        public long LargestPalindromeProductOfTwoNDigitNumbers(int n);
        public bool IsPalindrome(long n);
        public string Reverse(string s);
    }
}