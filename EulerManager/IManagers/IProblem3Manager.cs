﻿namespace EulerLogic.IManagers
{
    public interface IProblem3Manager
    {
        public ulong LargestPrimeFactorOfN(ulong n);
    }
}