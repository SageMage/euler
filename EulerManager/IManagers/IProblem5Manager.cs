﻿namespace EulerLogic.IManagers
{
    public interface IProblem5Manager
    {
        public ulong SmallestMultipleOfNumbersBetween1AndN(ulong n);
    }
}
