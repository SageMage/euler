﻿namespace EulerLogic.IManagers
{
    public interface IProblem1Manager
    {
        public long CalculateSumOfMultiples(int x, int y, int max);
    }
}