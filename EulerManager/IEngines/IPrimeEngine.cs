﻿using System.Collections.Generic;

namespace EulerLogic.IEngines
{
    public interface IPrimeEngine
    {
        public bool IsPrime(ulong number);
        public IEnumerable<ulong> GenerateEnumerableOfPrimes(ulong n);
    }
}