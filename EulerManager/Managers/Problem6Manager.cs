﻿using System;
using EulerLogic.IManagers;

namespace EulerLogic.Managers
{
    public class Problem6Manager : IProblem6Manager
    {
        public long DifferenceBetweenSumAndSquareOfFirstXNaturalNumbers(int x)
        {
            return SquareOfTheSumOfFirstXNaturalNumbers(x) - SumOfTheSquaresOfFirstXNaturalNumbers(x);
        }

        public long SumOfTheSquaresOfFirstXNaturalNumbers(int x)
        {
            double result = 0;
            for (var i = 1; i <= x; i++)
            {
                result += Math.Pow(i, 2);
            }

            return (long)result;
        }

        public long SquareOfTheSumOfFirstXNaturalNumbers(int x)
        {
            double result = 0;
            for (int i = 1; i <= x; i++)
            {
                result += i;
            }

            return (long) Math.Pow(result, 2);
        }
    }
}
