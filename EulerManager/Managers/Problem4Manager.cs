﻿using System;
using System.Collections.Generic;
using System.Linq;
using EulerLogic.IManagers;

namespace EulerLogic.Managers
{
    public class Problem4Manager : IProblem4Manager
    {
        public long LargestPalindromeProductOfTwoNDigitNumbers(int n)
        {
            var result = new List<long>();

            var largestNumberCharArray = new char[n];
            for (var index = 0; index < largestNumberCharArray.Length; index++)
            {
                largestNumberCharArray[index] = '9';
            }

            var largestNumber = long.Parse(new string(largestNumberCharArray));

            for (var i = largestNumber; i >= largestNumber / 2; i--)
            {
                for (var j = largestNumber; j >= largestNumber / 2; j--)
                {
                    var product = i * j;
                    if (IsPalindrome(product))
                        result.Add(product);
                }
            }

            return result.Max();
        }

        public bool IsPalindrome(long n)
        {
            var result = false;
            var reversed = Reverse(n.ToString());

            if (Equals(reversed, n.ToString()))
                result = true;

            return result;
        }

        public string Reverse(string s)
        {
            var charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}