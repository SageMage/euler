﻿using EulerLogic.IManagers;

namespace EulerLogic.Managers
{
    public class Problem1Manager : IProblem1Manager
    {
        public long CalculateSumOfMultiples(int x, int y, int max)
        {
            long result = 0 ;
            for (var i = 0; i < max; i++)
            {
                if (i % x == 0 || i % y == 0)
                {
                    result += i;
                }
            }

            return result;
        }
    }
}
