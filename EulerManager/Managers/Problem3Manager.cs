﻿using System;
using EulerLogic.IEngines;
using EulerLogic.IManagers;

namespace EulerLogic.Managers
{
    public class Problem3Manager : IProblem3Manager
    {
        private readonly IPrimeEngine _primeEngine;

        public Problem3Manager(IPrimeEngine primeEngine)
        {
            _primeEngine = primeEngine;
        }

        public ulong LargestPrimeFactorOfN(ulong n)
        {
            ulong lastFactor;
            if (n % 2 == 0)
            {
                n /= 2;
                lastFactor = 2;
                while (n % 2 == 0)
                {
                    n /= 2;
                }
            }
            else
            {
                lastFactor = 1;
            }

            ulong factor = 3;
            while (n > 1)
            {
                if (n % factor == 0)
                {
                    n /= factor;
                    lastFactor = factor;
                    while (n % factor == 0)
                        n /= factor;
                }
                factor += 2;
            }

            return lastFactor;
        }
    }
}
