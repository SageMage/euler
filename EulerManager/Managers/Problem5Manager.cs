﻿using EulerLogic.IManagers;

namespace EulerLogic.Managers
{
    public class Problem5Manager : IProblem5Manager
    {
        public ulong SmallestMultipleOfNumbersBetween1AndN(ulong n)
        {
            var foundSmallestMultiple = false;
            var numberToCheck = n; 

            while (!foundSmallestMultiple)
            {
                for (ulong i = 2; i <= n; i++)
                {
                    if (numberToCheck % i == 0)
                    {
                        if (i == n)
                        {
                            foundSmallestMultiple = true;
                        }
                    }
                    else //we've already failed so let's just end the loop
                    {
                        i = n + 1;
                    }
                }

                if (!foundSmallestMultiple)
                {
                    numberToCheck += n;
                }
            }

            return numberToCheck;
        }
    }
}
