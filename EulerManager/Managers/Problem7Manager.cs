﻿using System;
using System.Linq;
using EulerLogic.Engines;
using EulerLogic.IManagers;

namespace EulerLogic.Managers
{
    public class Problem7Manager : IProblem7Manager
    {
        public ulong NthPrime(ulong n)
        {
           return new PrimeEngine().GenerateEnumerableOfPrimes(n).Max();
        }
    }
}
