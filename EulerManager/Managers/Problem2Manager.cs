﻿using EulerLogic.IManagers;

namespace EulerLogic.Managers
{
    public class Problem2Manager : IProblem2Manager
    {
        public long SumEvenFibonacciNumbersLessThanN(int n)
        {
            long x = 1;
            long j = 1;
            long result = 0;

            while(x + j < n)
            {
                var y = j;
                j = x + j;
                x = y;
                if (j % 2 == 0)
                    result += j;
            }

            return result;
        }
    }
}