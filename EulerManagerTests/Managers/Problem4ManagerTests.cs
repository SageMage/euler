﻿using EulerLogic.Managers;
using Xunit;

namespace EulerLogicTests.Managers
{
    public class Problem4ManagerTests
    {
        private readonly Problem4Manager _classUnderTest;

        public Problem4ManagerTests()
        {
            _classUnderTest = new Problem4Manager();
        }

        [Theory]
        [InlineData(2, 9009)]
        [InlineData(3, 906609)]
        public void LargestPalindromeProductOfNDigits_ShouldSucceed(int n, long expected)
        {
            //Arrange
            
            //Act
            var actual = _classUnderTest.LargestPalindromeProductOfTwoNDigitNumbers(n);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1001, true)]
        public void IsPalindrome_ShouldSucceed(long n, bool expected)
        {
            //Arrange
            //Act
            var actual = _classUnderTest.IsPalindrome(n);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("test", "tset")]
        [InlineData("racecar", "racecar")]
        public void Reverse_ShouldReverseString(string input, string expected)
        {
            //Arrange
            //Act
            var actual = _classUnderTest.Reverse(input);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
