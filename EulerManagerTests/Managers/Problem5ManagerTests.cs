﻿using EulerLogic.Managers;
using Xunit;

namespace EulerLogicTests.Managers
{
    public class Problem5ManagerTests
    {
        private readonly Problem5Manager _classUnderTest;

        public Problem5ManagerTests()
        {
            _classUnderTest = new Problem5Manager();
        }

        [Theory]
        [InlineData(10, 2520)]
        [InlineData(20, 232792560)]
        public void SmallestMultipleOfNumbersBetween1AndN_ShouldSucceed(ulong n, ulong expected)
        {
            //Arrange
            //Act
            var actual = _classUnderTest.SmallestMultipleOfNumbersBetween1AndN(n);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
