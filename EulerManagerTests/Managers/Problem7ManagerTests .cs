using EulerLogic.Managers;
using Xunit;

namespace EulerLogicTests.Managers
{
    public class Problem7ManagerTests
    {
        private readonly Problem7Manager _classUnderTest;

        public Problem7ManagerTests()
        {
            _classUnderTest = new Problem7Manager();
        }

        [Theory]
        [InlineData(6, 13)]
        [InlineData(10001, 56183)]
        public void NthPrime_Succeeds(ulong n, ulong expected)
        {
            //Arrange

            //Act
            var actual = _classUnderTest.NthPrime(n);
            
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
