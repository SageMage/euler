﻿using EulerLogic.Engines;
using EulerLogic.IEngines;
using EulerLogic.Managers;
using Xunit;

namespace EulerLogicTests.Managers
{
    public class Problem3ManagerTests
    {
        private readonly Problem3Manager _classUnderTest;

        public Problem3ManagerTests()
        {
            IPrimeEngine primeEngine = new PrimeEngine();
            _classUnderTest = new Problem3Manager(primeEngine);
        }

        [Theory]
        [InlineData(6u, 3u)]
        [InlineData(13195, 29)]
        [InlineData(600851475143, 6857)]
        public void GetLargestPrimeFactor_ShouldSucceed(ulong x, ulong expected)
        {
            //Arrange
            
            //Act
            var actual = _classUnderTest.LargestPrimeFactorOfN(x);
            //Assert
            Assert.Equal(expected,actual);
        }
    }
}
