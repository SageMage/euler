﻿using EulerLogic.Managers;
using Xunit;

namespace EulerLogicTests.Managers
{
    public class Problem2ManagerTests
    {
        private readonly Problem2Manager _classUnderTest;

        public Problem2ManagerTests()
        {
            _classUnderTest = new Problem2Manager();
        }

        [Theory]
        [InlineData(100, 44)]
        [InlineData(4000000, 4613732)]
        public void SumEvenFibonacciNumbersLessThanN_ShouldSucceed(int n, int expected)
        {
            var actual = _classUnderTest.SumEvenFibonacciNumbersLessThanN(n);

            Assert.Equal(expected, actual);
        }
    }
}
