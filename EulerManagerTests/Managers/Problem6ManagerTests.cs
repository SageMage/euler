﻿using EulerLogic.Managers;
using Xunit;

namespace EulerLogicTests.Managers
{
    public class Problem6ManagerTests
    {
        private readonly Problem6Manager _classUnderTest;

        public Problem6ManagerTests()
        {
            _classUnderTest = new Problem6Manager();
        }

        [Theory]
        [InlineData(10, 2640)]
        [InlineData(100, 25164150)]
        public void DifferenceBetweenSumAndSquareOfFirstXNaturalNumbers_ShouldSucceed(int n, long expected)
        {
            //Arrange
            //Act
            var actual = _classUnderTest.DifferenceBetweenSumAndSquareOfFirstXNaturalNumbers(n);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(10, 385)]
        public void SumOfTheSquaresOfFirstXNaturalNumbers_ShouldSucceed(int n, long expected)
        {
            //Arrange
            //Act
            var actual = _classUnderTest.SumOfTheSquaresOfFirstXNaturalNumbers(n);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(10, 3025)]
        public void SquareOfTheSumOfFirstXNaturalNumbers_ShouldSucceed(int n, long expected)
        {
            //Arrange
            //Act
            var actual = _classUnderTest.SquareOfTheSumOfFirstXNaturalNumbers(n);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
