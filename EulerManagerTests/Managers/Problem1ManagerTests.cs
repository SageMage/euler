using EulerLogic.Managers;
using Xunit;

namespace EulerLogicTests.Managers
{
    public class Problem1ManagerTests
    {
        private readonly Problem1Manager _classUnderTest;

        public Problem1ManagerTests()
        {
            _classUnderTest = new Problem1Manager();
        }

        [Theory]
        [InlineData(3, 5, 10, 23)]
        [InlineData(3, 5, 1000, 233168)]
        public void CalculateSumOfMultiples_Succeeds(int x, int y, int max, int expected)
        {
            //Arrange

            //Act
            var actual = _classUnderTest.CalculateSumOfMultiples(x, y, max);
            
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
