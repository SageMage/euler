﻿using System.Linq;
using EulerLogic.Engines;
using Xunit;

namespace EulerLogicTests.Engines
{
    public class PrimeEngineTests
    {
        private readonly PrimeEngine _classUnderTest;

        public PrimeEngineTests()
        {
            _classUnderTest = new PrimeEngine();
        }

        [Theory]
        [InlineData(5u, true)]
        [InlineData(7u, true)]
        [InlineData(13u, true)]
        [InlineData(12u, false)]
        public void ShouldIndicateIfNumberIsPrime(ulong input, bool expected)
        {
            // Act - When
            var actual = _classUnderTest.IsPrime(input);

            // Assert - Then
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(6, 13)]
        [InlineData(10001, 56183)]
        public void GenerateEnumerableOfPrimes_NthIndexShouldBeExpected(ulong n, ulong expected)
        {
            //Act
            var actual = _classUnderTest.GenerateEnumerableOfPrimes(n).ToList();
            
            //Assert
            Assert.Equal(actual[(int)n - 1], expected);
        }
    }
}
